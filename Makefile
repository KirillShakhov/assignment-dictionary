ASM=nasm
ASMFLAGS=-f elf64
LD=ld

lab: main.o lib.o dict.o
	$(LD) -o $@ $^

main.o: main.asm colon.inc words.inc defines.inc

dict.o: dict.asm

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

