section .text
%include "defines.inc"

global exit
global string_length
global print_string
global print_string_err
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy


; Принимает код возврата и завершает текущий процесс
exit:
    mov rdi, rax
    mov rax, syscall_exit
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax

    .loop:
    cmp byte[rdi+rax], 0
    je .end
    inc rax
    jmp .loop

    .end:
    ret

print_string_err:
   mov rsi, stderr
   jmp print_str

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, new_line
print_string:
   mov rsi, stdout
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_str:
    push rdi
    push rsi
    call string_length
    pop rdi
    pop rsi
    mov rdx, rax
    mov rax, io_write
    syscall
    ret


; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdi, stdout
    mov rdx, 1; char count
    mov rax, io_write
    mov rsi, rsp
    syscall
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rsi, rsp
    push 0
    mov rax, rdi
    mov rdi, 10

    .loop:
    xor rdx, rdx
    div rdi
    add rdx, '0'
    dec rsp
    mov [rsp], dl
    cmp rax, 0
    je .out
    jmp .loop

    .out:
    mov rdi, rsp
    push rsi
    call print_string
    pop rsi
    mov rsp, rsi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jns .print
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .print:
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push r8

    push rdi
    call string_length
    pop rdi

    mov r8, rax
    xchg rdi, rsi

    push rsi
    push rdi
    call string_length
    pop rdi
    pop rsi

    cmp rax, r8
    jne .not_equal
    cmp rax, 0
    je .equal

    .check:
    mov r9b, byte[rdi]
    mov r10b, byte[rsi]
    cmp r9b, r10b
    jne .not_equal
    inc rdi
    inc rsi
    dec rax
    cmp rax, 0
    jne .check

    .equal:
    mov rax, 1
    jmp .end

    .not_equal:
    xor rax, rax


    .end:
    pop r8
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, io_read
    push rax
    mov rdi, stdin
    mov rsi, rsp
    mov rdx, 1; char count
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rdx, rdx
    xor r8, r8
    dec rsi

    .check:
    cmp r8, 0
    jne .success


    .read_char:
    push rdi
    push rsi
    push rdx
    call read_char
    pop rdx
    pop rsi
    pop rdi

    cmp rax, 0x20; пробел
    je .check
    cmp rax, 0x9; табуляция
    je .check
    cmp rax, 0xA; перевод строки
    je .check
    mov r8, 1
    jmp .check_length

    .check_length:
    cmp rdx, rsi
    ja .error
    test al, al
    jz .success
    mov byte[rdi+rdx], al
    inc rdx
    jmp .read_char
    .success:
    mov byte[rdi+rdx], 0
    mov rax, rdi
    ret
    .error:
    xor rax, rax
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    call string_length
    mov rdx, rax
    xor rcx, rcx
    xor rax, rax

    .loop:
    mov r9b, byte[rdi+rcx]
    cmp r9b, '0'
    jnge .end
    cmp r9b, '9'
    jnle .end
    sub r9b, '0'
    imul rax, 10
    add al, r9b
    inc rcx
    cmp rcx, rdx
    jne .loop
    ret

    .end:
    mov rdx, rcx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
    je .sign
    jmp parse_uint
    .sign:
    inc rdi
    call parse_uint
    test rdx, rdx
    je .end
    inc rdx
    neg rax
    .end:
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push rdi
    call string_length
    pop rdi

    inc rax
    xor rcx, rcx
    cmp rdx, rax
    jnge .error

    .loop:
    mov r9b, byte[rdi+rcx]
    mov byte[rsi+rcx], r9b
    inc rcx
    cmp rcx, rax
    jne .loop
    dec rax
    ret

    .error:
    xor rax, rax
    ret
