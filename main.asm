%include "words.inc"
%include "defines.inc"

section .data
	word_buf: times 255 db 0x00 ;  ;buffer size - 255
section .rodata
	not_found_mes: db "Not Found", 10, 0
	enter_key: db "Enter the key: ", 0
	value: db "Value: ", 0


global _start
extern find_word
extern print_string
extern print_string_err
extern exit
extern read_word


section .text

_start:
	mov rdi, enter_key
	call print_string

	mov rdi, word_buf
	mov rsi, 255 ;buffer size
	call read_word

	mov rdi, word_buf
	mov rsi, i5
	call find_word
	test rdi,rdi
	jz .not_found

    .found:
	push rdi
    mov rdi, value
    call print_string
    pop rdi
    call print_string
    xor rax, rax; return code
    call exit

	.not_found:
	    mov rdi, not_found_mes
    	call print_string_err
    	mov rax, 1; return code
    	call exit
